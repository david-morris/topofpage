# Top Of Page link on web pages#

The "Top Of Page" indicator is useful on a long webpage to allow the user to quickly navigate back to the top of the page. This works by using jQuery to add an anchor tag to the page which is initially set as invisible (display:none) and then made visible once the user scrolls down the page a specified number of pixels.

* Version 1.00

### Setup Information ###
* Include the CSS in the <head> section of your web page.
* Include a link to jQuery just before the </body> tag on your web page.
* Include a link to the JavaScript file just before the </body> tag on your web page.

### Configuration ###
The JavaScript file contains two variables which can be changed to suit your needs.

Variable **amountScrolled** determines the pixel height of the screen to be scrolled before the "Back to Top" indicator is shown.

Variable **duration** determines the length of time in milliseconds for the "Back to Top" indicator to fade in and out.

### Dependencies ###
Requires jQuery which can be linked via CDN or stored locally.